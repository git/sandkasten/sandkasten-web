import {
  LanguageDescription,
  LanguageSupport,
  StreamLanguage,
  StreamParser,
} from '@codemirror/language';
import { javascript } from '@codemirror/lang-javascript';

function legacy(parser: StreamParser<unknown>): LanguageSupport {
  return new LanguageSupport(StreamLanguage.define(parser));
}

export const CODE_DEFAULTS = {
  C: /** @lang C */ `#include <stdio.h>
int main() {
    printf("Hello, World!\\n");
    return 0;
}`,
  'C++': /** @lang C++ */ `#include <iostream>
int main() {
    std::cout << "Hello, World!\\n";
    return 0;
}`,
  JavaScript: /** @lang JS */ `console.log("Hello, World!");`,
  TypeScript: /** @lang TS */ `console.log("Hello, World!");`,
  Bash: 'echo "Hello, world!"',
};

export const LANGUAGES = [
  LanguageDescription.of({
    name: 'C',
    extensions: ['c', 'h', 'ino'],
    load() {
      return import('@codemirror/lang-cpp').then((m) => m.cpp());
    },
  }),
  LanguageDescription.of({
    name: 'C++',
    alias: ['cpp'],
    extensions: ['cpp', 'c++', 'cc', 'cxx', 'hpp', 'h++', 'hh', 'hxx'],
    load() {
      return import('@codemirror/lang-cpp').then((m) => m.cpp());
    },
  }),
  LanguageDescription.of({
    name: 'JavaScript',
    alias: ['ecmascript', 'js', 'node'],
    extensions: ['js', 'mjs', 'cjs'],
    support: javascript(),
  }),
  LanguageDescription.of({
    name: 'TypeScript',
    alias: ['ts'],
    extensions: ['ts'],
    load() {
      return import('@codemirror/lang-javascript').then((m) =>
        m.javascript({ typescript: true })
      );
    },
  }),
  LanguageDescription.of({
    name: 'Bash',
    alias: ['bash', 'sh', 'sh'],
    extensions: ['sh', 'ksh', 'bash'],
    filename: /^PKGBUILD$/,
    load() {
      return import('@codemirror/legacy-modes/mode/shell').then((m) =>
        legacy(m.shell)
      );
    },
  }),
];
