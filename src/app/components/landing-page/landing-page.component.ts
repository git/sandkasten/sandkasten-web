import { Component, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { ThemeService } from '../../services/theme.service';
import { TranslateModule } from '@ngx-translate/core';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
  standalone: true,
  imports: [NgClass, TranslateModule, RouterLink],
})
export class LandingPageComponent implements OnInit {
  themeClass!: string;

  constructor(
    private router: Router,
    private themeService: ThemeService
  ) {}

  ngOnInit() {
    this.themeService.isDarkTheme.subscribe((value) => {
      value
        ? (this.themeClass = 'dark-theme')
        : (this.themeClass = 'light-theme');
    });
  }
  // Si click sur "Run", on redirige vers la page d'édition
  onContinue(): void {
    this.router.navigateByUrl('/editor');
  }
}
