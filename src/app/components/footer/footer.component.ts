import { Component } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { RouterLink, RouterLinkActive } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  standalone: true,
  imports: [RouterLink, RouterLinkActive, TranslateModule],
})
export class FooterComponent {
  sandkasten_logo: string = 'assets/img/logo.png';
  twitter_logo: string = 'assets/img/twitter.svg';
  instagram_logo: string = 'assets/img/instagram.svg';
  facebook_logo: string = 'assets/img/facebook.svg';
}
