import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'app-terms-of-service',
  templateUrl: './terms-of-service.component.html',
  styleUrl: './terms-of-service.component.scss',
  standalone: true,
  imports: [TranslateModule],
})
export class TermsOfServiceComponent {
  constructor(private router: Router) {}

  // Si click sur "Run", on redirige vers la page de termes de service
  onContinue(): void {
    this.router.navigateByUrl('/terms-of-service');
  }
}
