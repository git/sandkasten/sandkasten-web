import { Component, OnInit } from '@angular/core';
import { Work } from '../../models/work.model';
import { WorkService } from '../../services/work.service';
import { NgForOf, SlicePipe } from '@angular/common';
import { FormsModule, NgForm } from '@angular/forms';
import { WorkListDetailComponent } from '../work-list-detail/work-list-detail.component';

@Component({
  selector: 'app-works-list',
  standalone: true,
  imports: [NgForOf, FormsModule, SlicePipe, WorkListDetailComponent],
  templateUrl: './works-list.component.html',
  styleUrl: './works-list.component.scss',
})
export class WorksListComponent implements OnInit {
  works: Work[] = [];

  // TODO - REMOVE WHEN USER MANAGEMENT DONE
  FAKE_USER_ID = 1;

  constructor(protected workService: WorkService) {}

  ngOnInit() {
    this.workService.getWorks().then((works: Work[]) => {
      works.map((work: Work) => {
        if (work.user_id === this.FAKE_USER_ID) {
          this.works.push(work);
        }
      });
    });
  }

  onSubmit(form: NgForm) {
    this.workService.saveWork(form);
  }
}
