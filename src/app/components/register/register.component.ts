import { Component } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import {
  FormControl,
  Validators,
  FormsModule,
  ReactiveFormsModule,
  NgForm,
  FormBuilder,
} from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { merge } from 'rxjs';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-auth',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
    CommonModule,
  ],
})
export class RegisterComponent {
  hide = true;

  registerForm = this.formBuilder.group({
    email: ['', Validators.required],
    login: ['', Validators.required],
    password: ['', Validators.required],
  });
  errorMessage = '';

  successRegister = '';
  errorRegister = '';

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder
  ) {
  }

  register() {
    const formRegisterValue = this.registerForm.value;

    this.userService
      .postUser(
        formRegisterValue.email!,
        formRegisterValue.login!,
        formRegisterValue.password!
      )
      .subscribe((response) => {
        console.log('response :', response);
        if (response.success) {
          this.successRegister = 'Votre compte a été créé avec succès.';
          this.errorRegister = '';
        } else {
          this.errorRegister =
            "L'inscription a échoué : un compte avec ce login existe déjà.";
          this.successRegister = '';
        }
      });
  }
}
