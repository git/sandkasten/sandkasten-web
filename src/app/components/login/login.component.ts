import { Component } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import {
  FormControl,
  Validators,
  FormsModule,
  ReactiveFormsModule,
  NgForm,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { merge } from 'rxjs';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-auth',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
    CommonModule,
  ],
})
export class LoginComponent {
  hide = true;

  loginForm = this.formBuilder.group({
    login: ['', Validators.required],
    password: ['', Validators.required],
  });

  errorMessage = '';

  successLogin = '';
  errorLogin = '';

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder
  ) {
  }

  loginAction() {
    const formValue = this.loginForm.value;

    this.userService
      .loginUser(formValue.login!, formValue.password!)
      .then((response) => {
        console.log('response :', response);
        if (response.success) {
          this.successLogin = 'Vous êtes connecté.';
          this.errorLogin = '';
        } else {
          this.errorLogin = "L'authentification a échoué.";
          this.successLogin = '';
        }
      });
  }
}
