import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrl: './privacy-policy.component.scss',
  standalone: true,
  imports: [TranslateModule],
})
export class PrivacyPolicyComponent {
  constructor(private router: Router) {}

  // Si click sur "Run", on redirige vers la page de politique de confidentialité
  onContinue(): void {
    this.router.navigateByUrl('/privacy-policy');
  }
}
