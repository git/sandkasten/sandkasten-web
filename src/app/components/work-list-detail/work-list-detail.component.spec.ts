import { ComponentFixture, TestBed } from '@angular/core/testing';
import { WorkListDetailComponent } from './work-list-detail.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

describe('WorkListDetailComponent', () => {
  let component: WorkListDetailComponent;
  let fixture: ComponentFixture<WorkListDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [WorkListDetailComponent, HttpClientModule, RouterModule.forRoot([])],
    }).compileComponents();

    fixture = TestBed.createComponent(WorkListDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
