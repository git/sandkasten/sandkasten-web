import { Component, Input } from '@angular/core';
import { Work } from '../../models/work.model';
import { RouterLink } from '@angular/router';
import { SlicePipe } from '@angular/common';

@Component({
  selector: 'app-work-list-detail',
  standalone: true,
  imports: [RouterLink, SlicePipe],
  templateUrl: './work-list-detail.component.html',
  styleUrl: './work-list-detail.component.scss',
})
export class WorkListDetailComponent {
  @Input() work?: Work;
}
