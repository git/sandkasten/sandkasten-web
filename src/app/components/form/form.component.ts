import { Component } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';

import emailjs from '@emailjs/browser';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  standalone: true,
  imports: [ReactiveFormsModule],
})
export class FormComponent {
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      from_name: [''],
      to_name: ['Sandkasten'],
      from_email: [''],
      subject: [''],
      message: [''],
    });
  }

  async send() {
    emailjs.init('cLIr6GuwhkrH1JFio');
    try {
      const response = await emailjs.send(
        'service_rvmwu94',
        'template_q0spe61',
        {
          from_name: this.form.value.from_name,
          to_name: this.form.value.to_name,
          message: this.form.value.message,
          from_email: this.form.value.from_email,
          subject: this.form.value.subject,
        }
      );

      console.log('Envoi du message:', response);

      alert('Message envoyé avec succès !');
      this.form.reset();
    } catch (error) {
      console.error("Erreur lors de l'envoi du message:", error);
    }
  }
}
