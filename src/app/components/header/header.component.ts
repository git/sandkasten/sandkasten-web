import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { TranslationService } from '../../services/translation.service';
import { ThemeService } from '../../services/theme.service';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { Router, RouterLink, RouterLinkActive } from '@angular/router';
import { NgClass, NgOptimizedImage } from '@angular/common';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  standalone: true,
  imports: [
    NgClass,
    RouterLink,
    RouterLinkActive,
    ReactiveFormsModule,
    TranslateModule,
    NgOptimizedImage,
  ],
})
export class HeaderComponent {
  title: string = 'Sandkasten';
  version: string = '1.0';
  isMenuOpen: boolean = false;
  isCheck: boolean = false;
  linkLastWork = ''; //peut-être à cast en string

  @ViewChild('menuRef') menuRef!: ElementRef;
  @Input() themeClass!: string;
  @Input() themeService!: ThemeService;

  // Instanciation du service pour les actions de traduction
  constructor(
    private router: Router,
    private translationService: TranslationService,
    private userService: UserService
  ) {}

  // Méthode pour changer la langue
  onLanguageChange(event: Event) {
    this.translationService.onLanguageChange(
      (event.target as HTMLSelectElement).value as 'fr' | 'en'
    );
  }

  toggleTheme() {
    this.themeService.toggleDarkTheme();
    this.themeService.isDarkTheme.subscribe((value) => {
      this.isCheck = value;
    });
  }

  openCloseMenu() {
    this.isMenuOpen = !this.isMenuOpen;

    if (this.isMenuOpen) {
      // Add an overflow to the body
      document.body.classList.add('no-scroll');
    } else {
      // Remove the overflow of the body
      document.body.classList.remove('no-scroll');
    }
  }

  onLastWorkLink(): void {
    const url = `/work/${this.linkLastWork}`;
    this.router.navigateByUrl(url);
  }

  // Logout
  logout() {
    this.userService.logoutUser().subscribe((response) => {
      if (response.success) {
        console.log('Logout success');
      } else {
        console.log('Logout error');
      }
    });
  }
}
