import { Routes } from '@angular/router';
import { EditorComponent } from './components/editor/editor.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { DocumentationComponent } from './components/documentation/documentation.component';
import { FormComponent } from './components/form/form.component';
import { TermsOfServiceComponent } from './components/terms-of-service/terms-of-service.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { WorksListComponent } from './components/works-list/works-list.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';

// Toutes les routes de l'application sont définies ici
export const routes: Routes = [
  { path: '', component: LandingPageComponent },
  { path: 'work/:work', component: EditorComponent },
  { path: 'works', component: WorksListComponent },
  { path: 'editor', component: EditorComponent },
  { path: 'editor-live/:idRoom', component: EditorComponent },
  { path: 'documentation', component: DocumentationComponent },
  { path: 'contact', component: FormComponent },
  { path: 'terms-of-service', component: TermsOfServiceComponent },
  { path: 'privacy-policy', component: PrivacyPolicyComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
];
