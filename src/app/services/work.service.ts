import { Injectable } from '@angular/core';
import { Work } from '../models/work.model';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { NgForm } from '@angular/forms';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class WorkService {
  constructor(private http: HttpClient) {}

  getWorks(): Promise<Work[]> {
    return fetch(`${environment.apiUrl}/works`, {
      method: 'GET',
      credentials: 'include',
    }).then((response) => response.json());
  }

  getWorkByLink(link: string): Observable<Work | null> {
    return this.http.get<Work>(`${environment.apiUrl}/works/${link}`);
  }

  saveWork(form: NgForm): void {
    const code = form.value.content;

    this.http.post(`${environment.apiUrl}/works/save`, code, { withCredentials: true });
  }

  postWork(code: string, language: string): Observable<string> {
    const body = {
      language,
      title: `Basic ${language}`,
      code,
    };

    return this.http.post<Work>(`${environment.apiUrl}/works`, body).pipe(map((work) => work.link));
  }

  updateWork(id: string, code: string, language: string): void {
    const body = {
      newContent: code,
      language: language,
    };
    this.http.put(`${environment.apiUrl}/works/${id}/content`, body, { withCredentials: true });
  }
}
