import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  postUser(
    email: string,
    login: string,
    password: string
  ): Observable<Response> {
    const body = {
      email: email,
      login: login,
      password: password,
      permissions: 0,
    };

    return this.http.post<Response>(`${environment.apiUrl}/users`, body);
  }

  loginUser(login: string, password: string): Promise<Response> {
    const body = {
      login: login,
      password: password,
    };

    return fetch(`${environment.apiUrl}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
      credentials: 'include',
    }).then((response) => response.json());
  }

  logoutUser(): Observable<Response> {
    return this.http.post<Response>(`${environment.apiUrl}/users/logout`, {
      withCredentials: true,
    });
  }
}

type Response = {
  success: boolean;
};
