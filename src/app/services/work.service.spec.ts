import { TestBed } from '@angular/core/testing';

import { WorkService } from './work.service';
import { HttpClientModule } from '@angular/common/http';

describe('WorkService', () => {
  let service: WorkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(WorkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
