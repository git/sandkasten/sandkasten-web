import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class TranslationService {
  get language() {
    return this.translate.currentLang;
  }

  constructor(private translate: TranslateService) {}

  onLanguageChange(language: 'fr' | 'en') {
    this.translate.use(language);
  }
}
