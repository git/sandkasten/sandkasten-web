import { Component, OnInit } from '@angular/core';
import { ThemeService } from './services/theme.service';
import { FooterComponent } from './components/footer/footer.component';
import { RouterOutlet } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { NgClass } from '@angular/common';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  standalone: true,
  imports: [NgClass, HeaderComponent, RouterOutlet, FooterComponent],
})
export class AppComponent implements OnInit {
  themeClass = 'light-theme';

  constructor(public themeService: ThemeService) {}

  ngOnInit() {
    this.themeService.isDarkTheme.subscribe((isDark) => {
      this.themeClass = isDark ? 'dark-theme' : 'light-theme';
      console.log('Theme class updated:', this.themeClass);
    });
  }

  toggleTheme() {
    this.themeService.toggleDarkTheme();
  }
}
