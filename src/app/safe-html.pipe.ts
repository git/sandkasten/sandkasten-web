import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'safeHTML',
  standalone: true,
})
export class SafeHTMLPipe implements PipeTransform {
  constructor(protected sanitizer: DomSanitizer) {}

  transform(value: unknown): unknown {
    return this.sanitizer.bypassSecurityTrustHtml(value as string);
  }
}
