import { SafeHTMLPipe } from './safe-html.pipe';
import { inject } from '@angular/core/testing';
import { DomSanitizer } from '@angular/platform-browser';

describe('SafeHTMLPipe', () => {
  it('create an instance', inject(
    [DomSanitizer],
    (domSanitizer: DomSanitizer) => {
      const pipe = new SafeHTMLPipe(domSanitizer);
      expect(pipe).toBeTruthy();
    }
  ));
});
