export interface Work {
  id_work: number;
  link: string;
  user_id: number;
  language: string;
  title: string;
  content: string;
}
