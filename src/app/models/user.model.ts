export interface User {
  id_user: number;
  login: string;
  password: string;
  permissions: number;
}
