export const environment = {
  production: false,
  apiUrl: 'http://localhost:3000',
  webSocketUrl: 'ws://localhost:3000',
};
