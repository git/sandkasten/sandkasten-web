export const environment = {
  production: true,
  apiUrl: 'https://tododomain.com',
  webSocketUrl: 'ws://tododomain.com',
};
